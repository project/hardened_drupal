; For all stable releases.
api = 2
core = 7.x
defaults[projects][subdir] = "contrib"

; Modules.
projects[acquia_connector] = 2.15
projects[autologout] = 4.3
projects[email_confirm] = 1.2
projects[genpass] = 1.0
projects[honeypot] = 1.17
projects[login_security] = 1.9
projects[mollom] = 2.13
projects[paranoia] = 1.4
projects[password_policy] = 1.11
projects[realname] = 1.2
projects[seckit] = 1.9
projects[tfa] = 2.0-beta1
projects[token] = 1.6
projects[username_enumeration_prevention] = 1.0