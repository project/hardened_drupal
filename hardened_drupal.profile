<?php
/**
 * @file
 * Enables modules and site configuration for a minimal site installation.
 */

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Allows the profile to alter the site configuration form.
 */
function hardened_drupal_form_install_configure_form_alter(&$form, $form_state) {
  // Pre-populate the site name with the server name.
  $form['site_information']['site_name']['#default_value'] = $_SERVER['SERVER_NAME'];

  $form['site_information']['acquia_identifier'] = array(
    '#title' => st('Acquia subscription identifier'),
    '#description' => st('If you have an <a href="@url">Acquia Network subscription</a>, please enter the subscription identifier. You can also provide it later at Administer > Configuration > Acquia Network settings.', array('@url' => 'http://network.acquia.com/network/dashboard/subscription')),
    '#type' => 'textfield',
    '#required' => FALSE,
  );
  $form['site_information']['acquia_key'] = array(
    '#title' => st('Acquia subscription key'),
    '#description' => st('If you have an <a href="@url">Acquia Network subscription</a>, please enter the subscription key. You can also provide it later at Administer > Configuration > Acquia Network settings.', array('@url' => 'http://network.acquia.com/network/dashboard/subscription')),
    '#type' => 'textfield',
    '#required' => FALSE,
  );

  // Add both existing submit function and our submit function,
  // since adding just ours cancels the automated discovery of the original.
  $form['#validate'] = array('hardened_drupal_configure_form_validate', 'install_configure_form_validate');
  $form['#submit'] = array('hardened_drupal_configure_form_submit', 'install_configure_form_submit');
}

/**
 * Custom validation handler for Hardened Drupal install.
 */
function hardened_drupal_configure_form_validate($form, &$form_state) {
  // Check credentials only if they provided.
  $form_state['values']['acquia_identifier'] = trim($form_state['values']['acquia_identifier']);
  $form_state['values']['acquia_key'] = trim($form_state['values']['acquia_key']);
  if (!empty($form_state['values']['acquia_identifier']) && !empty($form_state['values']['acquia_key'])) {
    if (!acquia_agent_valid_credentials($form_state['values']['acquia_identifier'], $form_state['values']['acquia_key'], acquia_agent_settings('acquia_network_address'))) {
      form_error($form, acquia_agent_connection_error_message());
    }
  }
}

/**
 * Custom submission handler for Hardened Drupal install.
 */
function hardened_drupal_configure_form_submit($form, &$form_state) {
  variable_set('acquia_identifier', $form_state['values']['acquia_identifier']);
  variable_set('acquia_key', $form_state['values']['acquia_key']);
}
