api = 2
core = 7.x
; We don't have any core patches, so just grab the standard release
projects[drupal][version] = 7.36
; Download the Hardened Drupal install profile and recursively build all its dependencies:
projects[hardened_drupal][version] = 1.x-dev
